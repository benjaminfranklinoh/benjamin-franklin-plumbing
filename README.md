Benjamin Franklin Plumbing has provided complete residential plumbing solutions in Toledo, OH since 1967. We provide a variety of plumbing services including: general plumbing, water heaters, water softening systems, pressure tanks, sump pumps, pipes & sewer, drains, and more. Call Today!

Address: 1602 W. Bancroft St, Toledo, OH 43606, USA

Phone: 419-473-6300

Website: https://www.benjaminfranklinplumbingtoledo.com
